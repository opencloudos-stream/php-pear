%global peardir %{_datadir}/pear
%global metadir %{_localstatedir}/lib/pear

%global getoptver 1.4.3
%global arctarver 1.4.14
%global structver 1.1.1
%global xmlutil   1.4.5
%global manpages  1.10.0

%global with_tests 0%{?_with_tests:1}

%{!?pecl_xmldir: %global pecl_xmldir %{_sharedstatedir}/php/peclxml}

Summary:  PHP Extension and Application Repository framework
Name:     php-pear
Version:  1.10.14
Release:  3%{?dist}
License:  BSD and LGPLv3+
URL:      http://pear.php.net/package/PEAR
Source0:  http://download.pear.php.net/package/PEAR-%{version}%{?pearprever}.tgz
Source1:  install-pear.php
Source2:  cleanup.php
Source3: pear.sh
Source4: pecl.sh
Source5: peardev.sh
Source6: macros.pear
Source7: http://pear.php.net/get/Archive_Tar-%{arctarver}.tgz
Source8: http://pear.php.net/get/Console_Getopt-%{getoptver}.tgz
Source9: http://pear.php.net/get/Structures_Graph-%{structver}.tgz
Source10: http://pear.php.net/get/XML_Util-%{xmlutil}.tgz
Source11: http://pear.php.net/get/PEAR_Manpages-%{manpages}.tgz

BuildRequires: php-common, php-cli, php-xml, gnupg2, php-devel
%if %{with_tests}
BuildRequires:  %{_bindir}/phpunit
%endif

Provides: php-pear(Console_Getopt) = %{getoptver}
Provides: php-pear(Archive_Tar) = %{arctarver}
Provides: php-pear(PEAR) = %{version}
Provides: php-pear(Structures_Graph) = %{structver}
Provides: php-pear(XML_Util) = %{xmlutil}
Provides: php-pear(PEAR_Manpages) = %{manpages}
Provides: php-composer(pear/console_getopt) = %{getoptver}
Provides: php-composer(pear/archive_tar) = %{arctarver}
Provides: php-composer(pear/pear-core-minimal) = %{version}
Provides: php-composer(pear/structures_graph) = %{structver}
Provides: php-composer(pear/xml_util) = %{xmlutil}

Requires:  php-common > 5.4
Requires:  php-cli
Requires:  php-posix
Requires:  php-xml
Requires:  httpd-filesystem

BuildArch: noarch

%description
PEAR is a framework and distribution system for reusable PHP
components.  This package contains the basic PEAR components.


%prep
%setup -cT

for archive in %{SOURCE0} %{SOURCE7} %{SOURCE8} %{SOURCE9} %{SOURCE10} %{SOURCE11}
do
    tar xzf  $archive --strip-components 1 || tar xzf  $archive --strip-path 1
    file=${archive##*/}
    [ -f LICENSE ] && mv LICENSE LICENSE-${file%%-*}
    [ -f README ]  && mv README  README-${file%%-*}

    tar xzf $archive 'package*xml'
    [ -f package2.xml ] && mv package2.xml ${file%%-*}.xml \
                        || mv package.xml  ${file%%-*}.xml
done
cp %{SOURCE1} .

sed -e 's:@BINDIR@:%{_bindir}:' \
    -e 's:@LIBDIR@:%{_localstatedir}/lib:' \
    %{SOURCE6} > macros.pear



%build



%install
export PHP_PEAR_SYSCONF_DIR=%{_sysconfdir}
export PHP_PEAR_SIG_KEYDIR=%{_sysconfdir}/pearkeys
export PHP_PEAR_SIG_BIN=%{_bindir}/gpg
export PHP_PEAR_INSTALL_DIR=%{peardir}
export PHP_PEAR_CACHE_DIR=${PWD}%{_localstatedir}/cache/php-pear
export PHP_PEAR_TEMP_DIR=/var/tmp

install -d %{buildroot}%{peardir} \
           %{buildroot}%{_localstatedir}/cache/php-pear \
           %{buildroot}%{_localstatedir}/www/html \
           %{buildroot}%{_localstatedir}/lib/pear/pkgxml \
           %{buildroot}%{_sysconfdir}/pear

export INSTALL_ROOT=%{buildroot}

%{_bindir}/php --version

%{_bindir}/php -dmemory_limit=64M -dshort_open_tag=0 -dsafe_mode=0 \
         -d 'error_reporting=E_ALL&~E_DEPRECATED' -ddetect_unicode=0 \
         install-pear.php --force \
                 --dir      %{peardir} \
                 --cache    %{_localstatedir}/cache/php-pear \
                 --config   %{_sysconfdir}/pear \
                 --bin      %{_bindir} \
                 --www      %{_localstatedir}/www/html \
                 --doc      %{_docdir}/pear \
                 --test     %{_datadir}/tests/pear \
                 --data     %{_datadir}/pear-data \
                 --metadata %{metadir} \
                 --man      %{_mandir} \
                 %{SOURCE0} %{SOURCE7} %{SOURCE8} %{SOURCE9} %{SOURCE10} %{SOURCE11}

install -m 755 %{SOURCE3} %{buildroot}%{_bindir}/pear
install -m 755 %{SOURCE4} %{buildroot}%{_bindir}/pecl
install -m 755 %{SOURCE5} %{buildroot}%{_bindir}/peardev

%{_bindir}/php %{SOURCE2} %{buildroot}%{_sysconfdir}/pear.conf %{_datadir}

%{_bindir}/php -r "print_r(unserialize(substr(file_get_contents('%{buildroot}%{_sysconfdir}/pear.conf'),17)));"


install -m 644 -D macros.pear \
           %{buildroot}%{_rpmconfigdir}/macros.d/macros.pear

pushd %{buildroot}%{peardir}
: no patch
popd

rm -rf %{buildroot}/.depdb* %{buildroot}/.lock %{buildroot}/.channels %{buildroot}/.filemap

install -m 644 *.xml %{buildroot}%{_localstatedir}/lib/pear/pkgxml



%check
grep %{buildroot} %{buildroot}%{_sysconfdir}/pear.conf && exit 1
grep %{_libdir} %{buildroot}%{_sysconfdir}/pear.conf && exit 1
grep '"/tmp"' %{buildroot}%{_sysconfdir}/pear.conf && exit 1
grep /usr/local %{buildroot}%{_sysconfdir}/pear.conf && exit 1
grep -rl %{buildroot} %{buildroot} && exit 1

%if %{with_tests}
cp /etc/php.ini .
echo "include_path=.:%{buildroot}%{peardir}:/usr/share/php" >>php.ini
export PHPRC=$PWD/php.ini
LOG=$PWD/rpmlog
ret=0

cd %{buildroot}%{_datadir}/tests/pear/Structures_Graph/tests
phpunit \
   AllTests || ret=1

cd %{buildroot}%{_datadir}/tests/pear/XML_Util/tests
phpunit \
   --bootstrap=/usr/share/pear/XML/Util/autoload.php \
   --test-suffix .php . || ret=1

cd %{buildroot}%{_datadir}/tests/pear/Console_Getopt/tests
%{_bindir}/php \
   %{buildroot}/usr/share/pear/pearcmd.php \
   run-tests \
   | tee -a $LOG

grep "FAILED TESTS" $LOG && ret=1

exit $ret
%else
echo 'Test suite disabled (missing "--with tests" option)'
%endif

%transfiletriggerin -- %{pecl_xmldir}
while read file; do
  %{_bindir}/pecl install --nodeps --soft --force --register-only --nobuild "$file" >/dev/null || :
done

%transfiletriggerun -- %{pecl_xmldir}
%{_bindir}/php -r '
while ($file=fgets(STDIN)) {
  $file = trim($file);
  $xml = simplexml_load_file($file);
  if (isset($xml->channel) &&  isset($xml->name)) {
    printf("%s/%s\n", $xml->channel, $xml->name);
  } else {
    fputs(STDERR, "Bad pecl package file ($file)\n");
  }
}' | while read  name; do
  %{_bindir}/pecl uninstall --nodeps --ignore-errors --register-only "$name" >/dev/null || :
done



%postun
if [ $1 -eq 0 -a -d %{metadir}/.registry ] ; then
  rm -rf %{metadir}/.registry
fi



%files
%license LICENSE*
%doc README*
%doc %{_docdir}/pear/*
%{_bindir}/*
%{peardir}
%dir %{metadir}
%{metadir}/.channels
%verify(not mtime size md5) %{metadir}/.depdb
%verify(not mtime)          %{metadir}/.depdblock
%verify(not mtime size md5) %{metadir}/.filemap
%verify(not mtime)          %{metadir}/.lock
%{metadir}/.registry
%{metadir}/pkgxml
%config(noreplace) %{_sysconfdir}/pear.conf
%{_rpmconfigdir}/macros.d/macros.pear
%dir %{_localstatedir}/cache/php-pear
%dir %{_sysconfdir}/pear
%dir %{_docdir}/pear
%{_datadir}/tests/pear
%{_datadir}/pear-data
%{_mandir}/man1/pear.1*
%{_mandir}/man1/pecl.1*
%{_mandir}/man1/peardev.1*
%{_mandir}/man5/pear.conf.5*


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.10.14-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.10.14-2
- Rebuilt for loongarch release

* Wed Jan 03 2024 Upgrade Robot <upbot@opencloudos.org> - 1.10.14-1
- Upgrade to version 1.10.14

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.10.13-5
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Sep 04 2023 Miaojun Dong <zoedong@tencent.com> - 1.10.13-4
- Rebuild for php-8.2.9

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.10.13-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.10.13-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Jan 9 2023 Zhao Zhen <jeremiazhao@tencent.com> - 1.10.13-1
- initial
